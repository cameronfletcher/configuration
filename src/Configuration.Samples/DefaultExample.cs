﻿namespace Configuration
{
    using System;
    using Configuration;

    public class DefaultExample
    {
        public void Configure()
        {
            // set up configuration provider(s)
            IConfigurationProvider configProvider = new DefaultConfiguration().CreateProvider();

            // get values from configuration
            var appSetting = configProvider.GetAppSetting("appSetting").AsString();
            var connectionString = configProvider.GetConnectionString("connectionString");

            // output
            Console.WriteLine(appSetting);
            Console.WriteLine(connectionString.Name);
        }
    }
}