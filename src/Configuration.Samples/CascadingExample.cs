﻿namespace Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using Configuration.Advanced;
    using Configuration.Sdk;

    public class CascadingExample
    {
        public void Configure()
        {
            var cannedConfig = new MemoryConfiguration().WithAppSetting("appSetting", "CASCADE!");
            var defaultConfig = new DefaultConfiguration();
            var configProvider = new CascadingConfiguration(first: cannedConfig, second: defaultConfig).CreateProvider();

            // get values from configuration
            var appSetting = configProvider.GetAppSetting("appSetting").AsString();
            var connectionString = configProvider.GetConnectionString("connectionString");

            // output
            Console.WriteLine(appSetting);
            Console.WriteLine(connectionString.Name);
        }
    }
}