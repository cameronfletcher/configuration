﻿namespace Configuration
{
    using System;
    using Configuration;

    public class MemoryExample
    {
        public void Configure()
        {
            // set up configuration provider(s)
            IConfigurationProvider configProvider = new MemoryConfiguration()
                .WithAppSetting("appSetting", "appSetting")
                .WithConnectionString("connectionString", "server=XYZ")
                .CreateProvider();

            // get values from configuration
            var appSetting = configProvider.GetAppSetting("appSetting").AsString();
            var connectionString = configProvider.GetConnectionString("connectionString");

            // output
            Console.WriteLine(appSetting);
            Console.WriteLine(connectionString.Name);
        }
    }
}