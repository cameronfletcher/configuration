﻿#pragma warning disable 0618
namespace Configuration
{
    using System;
    //// using System.Configuration // this line has been commented out
    using Configuration; // this line has been added

    public class LegacyExample
    {
        public void Configure()
        {
            // [optionally] set up the configuration provider (should be done once only at start of program)
            ConfigurationManager.Use<DefaultConfiguration>();
            //ConfigurationManager.Use(new FileConfiguration(@"C:\temp\app.config"));
            //ConfigurationManager.Use(() => new NetworkConfiguration(new Uri("http://test.com")));

            // get values from configuration
            var appSetting = ConfigurationManager.AppSettings["appSetting"];
            var connectionString = ConfigurationManager.ConnectionStrings["connectionString"];

            // output
            Console.WriteLine(appSetting);
            Console.WriteLine(connectionString.Name);
        }
    }
}
