﻿namespace Configuration
{
    using System;
    using Configuration;
    using Configuration.Advanced;
    using Configuration.Sdk;

    public class PersistentExample
    {
        public void Configure()
        {
            // set up configuration provider(s)
            IConfigurationProvider configProvider = new NetworkConfiguration(new Uri(""))
                .CacheToDisk("")
                .CreateProvider();

            // get values from configuration
            var appSettingN = configProvider.GetAppSetting("appSetting").AsString();
            appSettingN = configProvider.GetString("appSetting");
            var connectionStringN = configProvider.GetConnectionString("connectionString");

            // output
            //Console.WriteLine(appSettingN);
            //Console.WriteLine(connectionStringN.Name);
        }
    }
}