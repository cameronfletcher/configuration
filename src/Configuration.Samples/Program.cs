﻿namespace Configuration
{
    using System;
    using System.Configuration;
    using System.Globalization;
    using Configuration;

    public class FileCache
    {
        public FileCache(string filename)
        {
        }
    }

    internal class Program
    {
        internal static void Main(string[] args)
        {
            Console.WriteLine("new LegacyExample().Configure();");
            new LegacyExample().Configure();

            Console.WriteLine("new DefaultExample().Configure();");
            new DefaultExample().Configure();

            Console.WriteLine("new CascadingExample().Configure();");
            new CascadingExample().Configure();

            Console.WriteLine("new CannedExample().Configure();");
            new MemoryExample().Configure();

            // How can I use this?
            // How can convert a legacy application?
        }
    }
}
