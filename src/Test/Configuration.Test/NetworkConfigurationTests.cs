﻿// <copyright file="NetworkConfigurationTests.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Test
{
    using System;
    using System.IO;
    using System.Net;
    using Xunit;

    public sealed class NetworkConfigurationTests : IDisposable
    {
        private readonly HttpListener httpListener;

        // TODO (Cameron): Use new await and async keywords.
        public NetworkConfigurationTests()
        {
            // LINK (Cameron): http://stevenhollidge.blogspot.ch/2012/02/c-httplistener.html
            this.httpListener = new HttpListener();
            this.httpListener.Prefixes.Add("http://localhost:8123/");
            this.httpListener.Start();
            this.httpListener.BeginGetContext(
                result => 
                {
                    var buffer = File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "app.config"));
                    var context = this.httpListener.EndGetContext(result);
                    context.Response.StatusCode = (int)HttpStatusCode.OK;
                    context.Response.StatusDescription = HttpStatusCode.OK.ToString();
                    context.Response.ContentLength64 = buffer.Length;
                    context.Response.OutputStream.Write(buffer, 0, buffer.Length);
                    context.Response.OutputStream.Close();
                }, 
                this.httpListener);
        }

        [Fact]
        public void GetNetworkAppSetting()
        {
            // arrange
            var configUrl = new Uri("http://localhost:8123/config?application=ExampleApplication");
            var configProvider = new NetworkConfiguration(configUrl).CreateProvider();

            // act
            var appSetting = configProvider.GetAppSetting("appSetting").AsString();

            // assert
            Assert.Equal("appSettingValue", appSetting);
        }

        [Fact]
        public void GetNetworkConnectionString()
        {
            // arrange
            var configUrl = new Uri("http://localhost:8123/config?application=ExampleApplication");
            var configProvider = new NetworkConfiguration(configUrl).CreateProvider();

            // act
            var connectionSetting = configProvider.GetConnectionString("connectionString");

            // assert
            Assert.Equal("connectionString", connectionSetting.Name);
            Assert.Equal("Server=myServerAddress;Database=myDataBase;Trusted_Connection=True;", connectionSetting.Value);
            Assert.Equal("System.Data.SqlClient", connectionSetting.ProviderName);
        }

        public void Dispose()
        {
            this.httpListener.Close();
        }
    }
}
