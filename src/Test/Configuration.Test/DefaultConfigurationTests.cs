﻿// <copyright file="DefaultConfigurationTests.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Test
{
    using Xunit;

    public class DefaultConfigurationTests
    {
        [Fact]
        public void GetDefaultAppSetting()
        {
            // arrange
            var configProvider = new DefaultConfiguration().CreateProvider();

            // act
            var appSetting = configProvider.GetAppSetting("appSetting").AsString();

            // assert
            Assert.Equal("appSettingValue", appSetting);
        }

        [Fact]
        public void GetDefaultConnectionString()
        {
            // arrange
            var configProvider = new DefaultConfiguration().CreateProvider();

            // act
            var connectionSetting = configProvider.GetConnectionString("connectionString");

            // assert
            Assert.Equal("connectionString", connectionSetting.Name);
            Assert.Equal("Server=myServerAddress;Database=myDataBase;Trusted_Connection=True;", connectionSetting.Value);
            Assert.Equal("System.Data.SqlClient", connectionSetting.ProviderName);
        }
    }
}
