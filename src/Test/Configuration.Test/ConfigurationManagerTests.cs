﻿// <copyright file="ConfigurationManagerTests.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

#pragma warning disable 0618

namespace Configuration.Test
{
    using Configuration.Sdk;
    using FakeItEasy;
    using FluentAssertions;
    using Xunit;

    public class ConfigurationManagerTests
    {
        [Fact]
        public void GetAppSetting()
        {
            // arrange
            var appSetting = new DefaultAppSetting("key", "value");
            var config = A.Fake<IConfiguration>();
            var configProvider = A.Fake<IConfigurationProvider>();
            A.CallTo(() => config.CreateProvider()).Returns(configProvider);
            A.CallTo(() => configProvider.GetAppSetting(appSetting.Key, true)).Returns(appSetting);
            ConfigurationManager.Use(config);

            // act
            var appSettingValue = ConfigurationManager.AppSettings[appSetting.Key];

            // assert
            appSettingValue.Should().Be(appSetting.Value);
            A.CallTo(() => configProvider.GetAppSetting(appSetting.Key, true)).MustHaveHappened(Repeated.Exactly.Once);
        }

        [Fact]
        public void GetConnectionString()
        {
            // arrange
            var connectionString = new DefaultConnectionString("name", "value", "providerName");
            var config = A.Fake<IConfiguration>();
            var configProvider = A.Fake<IConfigurationProvider>();
            A.CallTo(() => config.CreateProvider()).Returns(configProvider);
            A.CallTo(() => configProvider.GetConnectionString(connectionString.Name, true)).Returns(connectionString);
            ConfigurationManager.Use(config);

            // act
            var connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionString.Name];

            // assert
            connectionStringSettings.Name.Should().Be(connectionString.Name);
            connectionStringSettings.ConnectionString.Should().Be(connectionString.Value);
            connectionStringSettings.ProviderName.Should().Be(connectionString.ProviderName);
            A.CallTo(() => configProvider.GetConnectionString(connectionString.Name, true)).MustHaveHappened(Repeated.Exactly.Once);
        }
    }
}
