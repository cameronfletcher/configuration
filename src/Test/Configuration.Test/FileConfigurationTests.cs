﻿// <copyright file="FileConfigurationTests.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Test
{
    using System;
    using System.IO;
    using Xunit;

    public class FileConfigurationTests
    {
        [Fact]
        public void InvalidFilename()
        {
            // arrange
            var configFile = Path.Combine(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString()), "app.config");
            
            // act (and assert)
            Assert.Throws<FileNotFoundException>(() => new FileConfiguration(configFile).CreateProvider());
        }

        [Fact]
        public void GetFileAppSetting()
        {
            // arrange
            var configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "app.config");
            var configProvider = new FileConfiguration(configFile).CreateProvider();

            // act
            var appSetting = configProvider.GetAppSetting("appSetting").AsString();

            // assert
            Assert.Equal("appSettingValue", appSetting);
        }

        [Fact]
        public void GetFileConnectionString()
        {
            // arrange
            var configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "app.config");
            var configProvider = new FileConfiguration(configFile).CreateProvider();

            // act
            var connectionSetting = configProvider.GetConnectionString("connectionString");

            // assert
            Assert.Equal("connectionString", connectionSetting.Name);
            Assert.Equal("Server=myServerAddress;Database=myDataBase;Trusted_Connection=True;", connectionSetting.Value);
            Assert.Equal("System.Data.SqlClient", connectionSetting.ProviderName);
        }
    }
}
