﻿// <copyright file="AppSettingExtensions.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using System.ComponentModel;
    using Configuration.Sdk;

    /// <summary>
    /// Provides extension methods for the <see cref="Configuration.IAppSetting"/> type.
    /// </summary>
    public static partial class AppSettingExtensions
    {
        /// <summary>
        /// Converts the application setting value to its formatted <see cref="System.String" /> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert and format.</param>
        /// <param name="args">A <see cref="System.Object"/> array containing zero or more objects to format.</param>
        /// <returns>Returns a formatted <see cref="System.String" /> value.</returns>
        public static string AsString(this IAppSetting appSetting, params object[] args)
        {
            return appSetting.AsString(args);
        }

        /// <summary>
        /// Converts the application setting value to its formatted <see cref="System.String" /> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert and format.</param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider" /> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <param name="args">A <see cref="System.Object"/> array containing zero or more objects to format.</param>
        /// <returns>Returns a formatted <see cref="System.String" /> value.</returns>
        public static string AsString(this IAppSetting appSetting, IFormatProvider provider, params object[] args)
        {
            Guard.Against.Null(() => appSetting);

            return string.Format(provider, appSetting.Value, args);
        }
    }
}
