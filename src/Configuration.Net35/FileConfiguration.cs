﻿// <copyright file="FileConfiguration.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System.Diagnostics.CodeAnalysis;
    using Configuration.Sdk;

    /// <summary>
    /// The file configuration.
    /// </summary>
    public class FileConfiguration : ConfigurationBase<FileConfiguration>, IConfiguration
    {
        private readonly string filename;

        /// <summary>
        /// Initialises a new instance of the <see cref="FileConfiguration" /> class.
        /// </summary>
        /// <param name="filename">The filename.</param>
        //// LINK (Cameron): http://stackoverflow.com/questions/2006795/fxcop-compound-word-should-be-treated-as-discrete-term
        [SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "filename", Justification = "This is a mess.")]
        public FileConfiguration(string filename)
        {
            Guard.Against.Null(() => filename);

            this.filename = filename;
        }

        /// <summary>
        /// Creates the configuration provider.
        /// </summary>
        /// <returns>The configuration provider.</returns>
        public IConfigurationProvider CreateProvider()
        {
            return new FileConfigurationProvider(new FileOperationsProxy(), this.filename, this.LogAppSetting, this.LogConnectionString);
        }
    }
}