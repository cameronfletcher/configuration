﻿// <copyright file="AppSettingExtensions.AsInt64.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using System.Globalization;
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.Int64"/> extension methods for the <see cref="Configuration.IAppSetting"/> type.
    /// </summary>
    public static partial class AppSettingExtensions
    {
        /// <summary>
        /// Converts the application setting value to its <see cref="System.Int64"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <returns>Returns a <see cref="System.Int64"/> value.</returns>
        public static long AsInt64(this IAppSetting appSetting)
        {
            return appSetting.AsInt64(CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Converts the application setting value to its <see cref="System.Int64"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <param name="styles">
        /// A bitwise combination of <see cref="System.Globalization.NumberStyles"/> values that indicates the
        /// style elements that can be present in the application setting value.
        /// </param>
        /// <returns>Returns a <see cref="System.Int64"/> value.</returns>
        public static long AsInt64(this IAppSetting appSetting, NumberStyles styles)
        {
            return appSetting.AsInt64(styles, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Converts the application setting value to its <see cref="System.Int64"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.Int64"/> value.</returns>
        public static long AsInt64(this IAppSetting appSetting, IFormatProvider provider)
        {
            return appSetting.AsInt64(NumberStyles.Any, provider);
        }

        /// <summary>
        /// Converts the application setting value to its <see cref="System.Int64"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <param name="styles">
        /// A bitwise combination of <see cref="System.Globalization.NumberStyles"/> values that indicates the
        /// style elements that can be present in the application setting value.
        /// </param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.Int64"/> value.</returns>
        public static long AsInt64(this IAppSetting appSetting, NumberStyles styles, IFormatProvider provider)
        {
            Guard.Against.Null(() => appSetting);

            return long.Parse(appSetting.Value, styles, provider);
        }
    }
}
