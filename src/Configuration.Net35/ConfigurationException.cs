﻿// <copyright file="ConfigurationException.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// The exception that is thrown when there is a problem with configuration.
    /// </summary>
    [Serializable]
    public class ConfigurationException : Exception, ISerializable
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigurationException" /> class.
        /// </summary>
        public ConfigurationException()
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigurationException" /> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ConfigurationException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigurationException" /> class.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public ConfigurationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigurationException" /> class.
        /// </summary>
        /// <param name="info">
        /// The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object 
        /// data about the exception being thrown.
        /// </param>
        /// <param name="context">
        /// The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information 
        /// about the source or destination.
        /// </param>
        protected ConfigurationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
