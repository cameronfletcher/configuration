﻿// <copyright file="DefaultConfiguration.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using Configuration.Sdk;

    /// <summary>
    /// The default configuration.
    /// </summary>
    public class DefaultConfiguration : ConfigurationBase<DefaultConfiguration>, IConfiguration
    {
        /// <summary>
        /// Creates the configuration provider.
        /// </summary>
        /// <returns>The configuration provider.</returns>
        public IConfigurationProvider CreateProvider()
        {
            return new FileConfigurationProvider(
                new FileOperationsProxy(), 
                AppDomain.CurrentDomain.SetupInformation.ConfigurationFile,
                this.LogAppSetting,
                this.LogConnectionString);
        }
    }
}