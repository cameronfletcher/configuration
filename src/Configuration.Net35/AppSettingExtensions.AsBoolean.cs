﻿// <copyright file="AppSettingExtensions.AsBoolean.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.Boolean"/> extension methods for the <see cref="Configuration.IAppSetting"/> type.
    /// </summary>
    public static partial class AppSettingExtensions
    {
        /// <summary>
        /// Converts the application setting value to its <see cref="System.Boolean"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <returns>Returns a <see cref="System.Boolean"/> value.</returns>
        public static bool AsBoolean(this IAppSetting appSetting)
        {
            Guard.Against.Null(() => appSetting);

            return bool.Parse(appSetting.Value);
        }
    }
}
