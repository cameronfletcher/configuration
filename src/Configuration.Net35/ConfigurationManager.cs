﻿// <copyright file="ConfigurationManager.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using Configuration.Sdk;

    /// <summary>
    /// Provides access to configuration data for applications.
    /// </summary>
    [Obsolete("Change your code to use IConfiguration instead.")]
    public static class ConfigurationManager
    {
        static ConfigurationManager()
        {
            Use<DefaultConfiguration>();
        }

        /// <summary>
        /// Gets the application settings for the current configuration provider.
        /// </summary>
        /// <value>The application settings.</value>
        public static AppSettings AppSettings { get; private set; }

        /// <summary>
        /// Gets the connection strings for the current configuration provider.
        /// </summary>
        /// <value>The connection strings.</value>
        public static ConnectionStringSettings ConnectionStrings { get; private set; }

        /// <summary>
        /// Uses the specified configuration type.
        /// </summary>
        /// <typeparam name="T">The configuration type.</typeparam>
        public static void Use<T>() where T : IConfiguration, new()
        {
            Use(new T());
        }

        /// <summary>
        /// Uses the specified configuration.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public static void Use(IConfiguration configuration)
        {
            Guard.Against.Null(() => configuration);

            var configProvider = configuration.CreateProvider();

            AppSettings = new AppSettings(configProvider);
            ConnectionStrings = new ConnectionStringSettings(configProvider);
        }
    }
}
