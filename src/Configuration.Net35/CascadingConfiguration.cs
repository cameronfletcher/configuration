﻿// <copyright file="CascadingConfiguration.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Configuration.Sdk;

    /// <summary>
    /// The cascading configuration.
    /// </summary>
    public class CascadingConfiguration : ConfigurationBase<CascadingConfiguration>, IConfiguration
    {
        private readonly List<IConfiguration> configurations;

        /// <summary>
        /// Initialises a new instance of the <see cref="CascadingConfiguration" /> class.
        /// </summary>
        /// <param name="first">The first configuration.</param>
        public CascadingConfiguration(IConfiguration first)
            : this(new[] { first })
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CascadingConfiguration" /> class.
        /// </summary>
        /// <param name="first">The first configuration.</param>
        /// <param name="second">The second configuration.</param>
        public CascadingConfiguration(IConfiguration first, IConfiguration second)
            : this(new[] { first, second })
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CascadingConfiguration" /> class.
        /// </summary>
        /// <param name="first">The first configuration.</param>
        /// <param name="second">The second configuration.</param>
        /// <param name="third">The third configuration.</param>
        public CascadingConfiguration(IConfiguration first, IConfiguration second, IConfiguration third)
            : this(new[] { first, second, third })
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CascadingConfiguration" /> class.
        /// </summary>
        /// <param name="first">The first configuration.</param>
        /// <param name="second">The second configuration.</param>
        /// <param name="third">The third configuration.</param>
        /// <param name="fourth">The fourth configuration.</param>
        public CascadingConfiguration(IConfiguration first, IConfiguration second, IConfiguration third, IConfiguration fourth)
            : this(new[] { first, second, third, fourth })
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CascadingConfiguration" /> class.
        /// </summary>
        /// <param name="first">The first configuration.</param>
        /// <param name="second">The second configuration.</param>
        /// <param name="third">The third configuration.</param>
        /// <param name="fourth">The fourth configuration.</param>
        /// <param name="fifth">The fifth configuration.</param>
        public CascadingConfiguration(IConfiguration first, IConfiguration second, IConfiguration third, IConfiguration fourth, IConfiguration fifth)
            : this(new[] { first, second, third, fourth, fifth })
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="CascadingConfiguration" /> class.
        /// </summary>
        /// <param name="inOrder">The configurations in order of preference.</param>
        public CascadingConfiguration(IEnumerable<IConfiguration> inOrder)
        {
            Guard.Against.NullOrEmptyOrNullElements(() => inOrder);

            this.configurations = new List<IConfiguration>(inOrder);
        }

        /// <summary>
        /// Creates the configuration provider.
        /// </summary>
        /// <returns>The configuration provider.</returns>
        public IConfigurationProvider CreateProvider()
        {
            return new CascadingConfigurationProvider(
                this.configurations.Select(configuration => configuration.CreateProvider()), 
                this.LogAppSetting, 
                this.LogConnectionString);
        }

        /// <summary>
        /// Adds another configuration to the list of configurations to cascade through.
        /// </summary>
        /// <param name="configuration">The configuration to add.</param>
        /// <returns>The cascading configuration.</returns>
        public CascadingConfiguration Add(IConfiguration configuration)
        {
            Guard.Against.Null(() => configuration);

            this.configurations.Add(configuration);

            return this;
        }
    }
}