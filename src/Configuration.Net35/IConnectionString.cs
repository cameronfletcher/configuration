﻿// <copyright file="IConnectionString.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using Configuration.Sdk;

    /// <summary>
    /// Exposes the public members of a connection string.
    /// </summary>
    public interface IConnectionString : IFluentExtension
    {
        /// <summary>
        /// Gets the name of the connection string.
        /// </summary>
        /// <value>The name of the connection string.</value>
        string Name { get; }

        /// <summary>
        /// Gets the connection string value.
        /// </summary>
        /// <value>The connection string value.</value>
        string Value { get; }

        /// <summary>
        /// Gets the name of the provider for the connection string.
        /// </summary>
        /// <value>The name of the provider for the connection string.</value>
        string ProviderName { get; }
    }
}
