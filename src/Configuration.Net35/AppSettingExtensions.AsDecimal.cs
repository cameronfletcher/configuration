﻿// <copyright file="AppSettingExtensions.AsDecimal.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using System.Globalization;
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.Decimal"/> extension methods for the <see cref="Configuration.IAppSetting"/> type.
    /// </summary>
    public static partial class AppSettingExtensions
    {
        /// <summary>
        /// Converts the application setting value to its <see cref="System.Decimal"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <returns>Returns a <see cref="System.Decimal"/> value.</returns>
        public static decimal AsDecimal(this IAppSetting appSetting)
        {
            return appSetting.AsDecimal(CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Converts the application setting value to its <see cref="System.Decimal"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <param name="styles">
        /// A bitwise combination of <see cref="System.Globalization.NumberStyles"/> values that indicates the
        /// style elements that can be present in the application setting value.
        /// </param>
        /// <returns>Returns a <see cref="System.Decimal"/> value.</returns>
        public static decimal AsDecimal(this IAppSetting appSetting, NumberStyles styles)
        {
            return appSetting.AsDecimal(styles, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Converts the application setting value to its <see cref="System.Decimal"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.Decimal"/> value.</returns>
        public static decimal AsDecimal(this IAppSetting appSetting, IFormatProvider provider)
        {
            return appSetting.AsDecimal(NumberStyles.Any, provider);
        }

        /// <summary>
        /// Converts the application setting value to its <see cref="System.Decimal"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <param name="styles">
        /// A bitwise combination of <see cref="System.Globalization.NumberStyles"/> values that indicates the
        /// style elements that can be present in the application setting value.
        /// </param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.Decimal"/> value.</returns>
        public static decimal AsDecimal(this IAppSetting appSetting, NumberStyles styles, IFormatProvider provider)
        {
            Guard.Against.Null(() => appSetting);

            return decimal.Parse(appSetting.Value, styles, provider);
        }
    }
}
