﻿// <copyright file="CascadingConfigurationProvider.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A cascading configuration provider.
    /// </summary>
    /// <remarks>This configuration provider inspects each of its configuration providers in order for a valid result.</remarks>
    public class CascadingConfigurationProvider : ConfigurationProviderBase
    {
        private readonly List<IConfigurationProvider> configurationProviders;

        /// <summary>
        /// Initialises a new instance of the <see cref="CascadingConfigurationProvider" /> class.
        /// </summary>
        /// <param name="configurationProviders">The configuration providers.</param>
        /// <param name="logAppSetting">The application setting logging action.</param>
        /// <param name="logConnectionString">The connection string logging action.</param>
        public CascadingConfigurationProvider(
            IEnumerable<IConfigurationProvider> configurationProviders, 
            Action<IAppSetting> logAppSetting, 
            Action<IConnectionString> logConnectionString)
            : base(logAppSetting, logConnectionString)
        {
            Guard.Against.NullOrEmptyOrNullElements(() => configurationProviders);

            this.configurationProviders = new List<IConfigurationProvider>(configurationProviders);
        }

        /// <summary>
        /// Gets the application setting from the configuration.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>The application setting.</returns>
        protected override IAppSetting GetAppSettingFromConfiguration(string key)
        {
            foreach (var configurationProvider in this.configurationProviders)
            {
                var appSetting = configurationProvider.GetAppSetting(key, true);
                if (appSetting != null)
                {
                    return appSetting;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the connection string from the configuration.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <returns>The connection string.</returns>
        protected override IConnectionString GetConnectionStringFromConfiguration(string name)
        {
            foreach (var configurationProvider in this.configurationProviders)
            {
                var connectionString = configurationProvider.GetConnectionString(name, true);
                if (connectionString != null)
                {
                    return connectionString;
                }
            }

            return null;
        }
    }
}