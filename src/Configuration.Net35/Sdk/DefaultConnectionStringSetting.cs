﻿// <copyright file="DefaultConnectionStringSetting.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// The default type of connection string setting.
    /// </summary>
    /// <remarks>Provided for backwards compatibility with .NET configuration only.</remarks>
    [Obsolete("Provided for backwards compatibility with .NET configuration only.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class DefaultConnectionStringSetting : IConnectionStringSetting
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="DefaultConnectionStringSetting" /> class.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <param name="connectionString">The connection string value.</param>
        /// <param name="providerName">Name of the provider.</param>
        public DefaultConnectionStringSetting(string name, string connectionString, string providerName)
        {
            Guard.Against.Null(() => name);
            Guard.Against.Null(() => connectionString);

            this.Name = name;
            this.ConnectionString = connectionString;
            this.ProviderName = providerName;
        }

        /// <summary>
        /// Gets the name of the connection string.
        /// </summary>
        /// <value>The name of the connection string.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the connection string value.
        /// </summary>
        /// <value>The connection string value.</value>
        public string ConnectionString { get; private set; }

        /// <summary>
        /// Gets the name of the provider for the connection string.
        /// </summary>
        /// <value>The name of the provider for the connection string.</value>
        public string ProviderName { get; private set; }
    }
}
