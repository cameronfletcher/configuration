﻿// <copyright file="FileOperationsProxy.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System.IO;

    /// <summary>
    /// The file operations proxy.
    /// </summary>
    public sealed class FileOperationsProxy : IFileOperationsProxy
    {
        /// <summary>
        /// Creates a uniquely named, zero-byte temporary file on disk and returns the full path of that file.
        /// </summary>
        /// <returns>A <see cref="System.String"/> containing the full path of the temporary file.</returns>
        public string GetTempFileName()
        {
            return Path.GetTempFileName();
        }

        /// <summary>
        /// Determines whether the specified file exists.
        /// </summary>
        /// <param name="path">The file to check.</param>
        /// <returns>Returns <c>true</c> if the path contains the name of an existing file; otherwise, <c>false</c>.</returns>
        public bool FileExists(string path)
        {
            return File.Exists(path);
        }

        /// <summary>
        /// Copies an existing file to a new file. Overwriting a file of the same name is allowed.
        /// </summary>
        /// <param name="sourceFilename">The file to copy.</param>
        /// <param name="destinationFilename">The name of the destination file. This cannot be a directory.</param>
        /// <param name="overwrite">Set to <c>true</c> if the destination file can be overwritten; otherwise, <c>false</c>.</param>
        public void FileCopy(string sourceFilename, string destinationFilename, bool overwrite)
        {
            File.Copy(sourceFilename, destinationFilename, overwrite);
        }
    }
}
