﻿// <copyright file="ConfigurationProviderBase.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;
    using System.Collections.Concurrent;
    using System.Globalization;

    /// <summary>
    /// The configuration provider base.
    /// </summary>
    public abstract class ConfigurationProviderBase : IConfigurationProvider
    {
        private readonly ConcurrentDictionary<string, IConnectionString> connectionStrings = new ConcurrentDictionary<string, IConnectionString>();
        private readonly ConcurrentDictionary<string, IAppSetting> appSettings = new ConcurrentDictionary<string, IAppSetting>();

        private readonly Action<IAppSetting> logAppSetting;
        private readonly Action<IConnectionString> logConnectionString;

        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigurationProviderBase" /> class.
        /// </summary>
        /// <param name="logAppSetting">The application setting logging action.</param>
        /// <param name="logConnectionString">The connection string logging action.</param>
        protected ConfigurationProviderBase(Action<IAppSetting> logAppSetting, Action<IConnectionString> logConnectionString)
        {
            Guard.Against.Null(() => logAppSetting);
            Guard.Against.Null(() => logConnectionString);

            this.logAppSetting = logAppSetting;
            this.logConnectionString = logConnectionString;
        }

        /// <summary>
        /// Gets the application setting.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>The application setting.</returns>
        public IAppSetting GetAppSetting(string key)
        {
            return this.GetAppSetting(key, false);
        }

        /// <summary>
        /// Gets the application setting.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="allowNull">If set to <c>true</c> then allows a null application setting to be returned.</param>
        /// <returns>The application setting.</returns>
        public IAppSetting GetAppSetting(string key, bool allowNull)
        {
            Guard.Against.Null(() => key);

            IAppSetting appSetting;
            if (this.appSettings.TryGetValue(key, out appSetting))
            {
                return appSetting;
            }

            appSetting = this.GetAppSettingFromConfiguration(key);
            if (appSetting == null && !allowNull)
            {
                throw new ConfigurationException(
                    string.Format(CultureInfo.InvariantCulture, "Cannot locate application setting with key '{0}' in configuration.", key));
            }

            this.appSettings.TryAdd(key, appSetting);
            this.logAppSetting(appSetting);

            return appSetting;
        }

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <returns>The connection string.</returns>
        public IConnectionString GetConnectionString(string name)
        {
            return this.GetConnectionString(name, false);
        }

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <param name="allowNull">If set to <c>true</c> then allows a null connection string to be returned.</param>
        /// <returns>The connection string.</returns>
        public IConnectionString GetConnectionString(string name, bool allowNull)
        {
            Guard.Against.Null(() => name);

            IConnectionString connectionString;
            if (this.connectionStrings.TryGetValue(name, out connectionString))
            {
                return connectionString;
            }

            connectionString = this.GetConnectionStringFromConfiguration(name);
            if (connectionString == null && !allowNull)
            {
                throw new ConfigurationException(
                    string.Format(CultureInfo.InvariantCulture, "Cannot locate connection string with name '{0}' in configuration.", name));
            }

            this.connectionStrings.TryAdd(name, connectionString);
            this.logConnectionString(connectionString);

            return connectionString;
        }

        /// <summary>
        /// Gets the application setting from the configuration.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>The application setting.</returns>
        protected abstract IAppSetting GetAppSettingFromConfiguration(string key);

        /// <summary>
        /// Gets the connection string from the configuration.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <returns>The connection string.</returns>
        protected abstract IConnectionString GetConnectionStringFromConfiguration(string name);
    }
}
