﻿// <copyright file="IWebOperationsProxy.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;

    /// <summary>
    /// Exposes the public members of web operations proxy.
    /// </summary>
    public interface IWebOperationsProxy
    {
        /// <summary>
        /// Downloads the resource with the specified URI to a local file.
        /// </summary>
        /// <param name="address">The URI from which to download data.</param>
        /// <param name="fileName">The name of the local file that is to receive the data.</param>
        void DownloadFile(Uri address, string fileName);
    }
}
