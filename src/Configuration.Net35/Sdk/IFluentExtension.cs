﻿// <copyright file="IFluentExtension.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

#pragma warning disable 1591

namespace Configuration.Sdk
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;

    //// LINK (Cameron): http://stackoverflow.com/questions/2945508/visual-studio-intellisense-ihideobjectmembers-trick-doesnt-work-what-am-i-mi
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Not editor browsable.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IFluentExtension
    {
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Justification = "Not my call.")]
        [SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "GetType", Justification = "This is it.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        Type GetType();

        [EditorBrowsable(EditorBrowsableState.Never)]
        int GetHashCode();

        [EditorBrowsable(EditorBrowsableState.Never)]
        string ToString();

        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "obj", Justification = "Not my call")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        bool Equals(object obj);
    }
}
