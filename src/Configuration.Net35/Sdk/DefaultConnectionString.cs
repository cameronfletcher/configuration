﻿// <copyright file="DefaultConnectionString.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    /// <summary>
    /// The default type of connection string.
    /// </summary>
    public class DefaultConnectionString : IConnectionString
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="DefaultConnectionString" /> class.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <param name="value">The connection string value.</param>
        /// <param name="providerName">The name of the provider for the connection string.</param>
        public DefaultConnectionString(string name, string value, string providerName)
        {
            Guard.Against.Null(() => name);
            Guard.Against.Null(() => value);

            this.Name = name;
            this.Value = value;
            this.ProviderName = providerName;
        }

        /// <summary>
        /// Gets the name of the connection string.
        /// </summary>
        /// <value>The name of the connection string.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the connection string value.
        /// </summary>
        /// <value>The connection string value.</value>
        public string Value { get; private set; }

        /// <summary>
        /// Gets the name of the provider for the connection string.
        /// </summary>
        /// <value>The name of the provider for the connection string.</value>
        public string ProviderName { get; private set; }
    }
}
