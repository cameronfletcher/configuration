﻿// <copyright file="FileConfigurationProvider.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;
    using System.Configuration;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using configurationManager = System.Configuration.ConfigurationManager;

    /// <summary>
    /// A file configuration provider.
    /// </summary>
    public class FileConfigurationProvider : ConfigurationProviderBase
    {
        private readonly Configuration configuration;

        /// <summary>
        /// Initialises a new instance of the <see cref="FileConfigurationProvider" /> class.
        /// </summary>
        /// <param name="fileOperationsProxy">The file operations proxy.</param>
        /// <param name="filename">The configuration filename.</param>
        /// <param name="logAppSetting">The application setting logging action.</param>
        /// <param name="logConnectionString">The connection string logging action.</param>
        [SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "filename", Justification = "This is a mess.")]
        public FileConfigurationProvider(IFileOperationsProxy fileOperationsProxy, string filename, Action<IAppSetting> logAppSetting, Action<IConnectionString> logConnectionString)
            : base(logAppSetting, logConnectionString)
        {
            Guard.Against.Null(() => fileOperationsProxy);

            if (!fileOperationsProxy.FileExists(filename))
            {
                throw new FileNotFoundException("Cannot find configuration file.", filename);
            }

            this.configuration = configurationManager.OpenMappedExeConfiguration(
                new ExeConfigurationFileMap { ExeConfigFilename = filename },
                ConfigurationUserLevel.None);
        }

        /// <summary>
        /// Gets the application setting from the configuration.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>The application setting.</returns>
        protected override IAppSetting GetAppSettingFromConfiguration(string key)
        {
            var setting = this.configuration.AppSettings.Settings[key];
            return setting == null ? null : new DefaultAppSetting(setting.Key, setting.Value);
        }

        /// <summary>
        /// Gets the connection string from the configuration.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <returns>The connection string.</returns>
        protected override IConnectionString GetConnectionStringFromConfiguration(string name)
        {
            var setting = this.configuration.ConnectionStrings.ConnectionStrings[name];
            return setting == null ? null : new DefaultConnectionString(setting.Name, setting.ConnectionString, setting.ProviderName);
        }
    }
}