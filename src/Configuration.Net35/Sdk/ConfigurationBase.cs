﻿// <copyright file="ConfigurationBase.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;

    /// <summary>
    /// The configuration base.
    /// </summary>
    /// <typeparam name="T">The type of configuration.</typeparam>
    public abstract class ConfigurationBase<T>
        where T : ConfigurationBase<T>
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ConfigurationBase{T}" /> class.
        /// </summary>
        protected ConfigurationBase()
        {
            this.LogAppSetting = appSetting => { };
            this.LogConnectionString = connectionString => { };
        }

        /// <summary>
        /// Gets the application setting logging action.
        /// </summary>
        /// <value>The application setting logging action.</value>
        protected Action<IAppSetting> LogAppSetting { get; private set; }

        /// <summary>
        /// Gets the connection string logging action.
        /// </summary>
        /// <value>The connection string logging action.</value>
        protected Action<IConnectionString> LogConnectionString { get; private set; }

        /// <summary>
        /// Logs using the specified logging action.
        /// </summary>
        /// <param name="log">The logging action.</param>
        /// <returns>The configuration.</returns>
        public T LogUsing(Action<string, string> log)
        {
            Guard.Against.Null(() => log);

            this.LogUsing(
                appSetting => log(appSetting.Key, appSetting.Value),
                connectionString => log(connectionString.Name, connectionString.Value));

            return this as T;
        }

        /// <summary>
        /// Logs using the specified logging actions.
        /// </summary>
        /// <param name="logAppSetting">The application setting logging action.</param>
        /// <param name="logConnectionString">The connection string logging action.</param>
        /// <returns>The configuration.</returns>
        public virtual T LogUsing(Action<IAppSetting> logAppSetting, Action<IConnectionString> logConnectionString)
        {
            Guard.Against.Null(() => logAppSetting);
            Guard.Against.Null(() => logConnectionString);

            this.LogAppSetting = logAppSetting;
            this.LogConnectionString = logConnectionString;
            
            return this as T;
        }
    }
}
