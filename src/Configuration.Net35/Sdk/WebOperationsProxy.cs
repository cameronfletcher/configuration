﻿// <copyright file="WebOperationsProxy.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;
    using System.Net;

    /// <summary>
    /// The web operations proxy.
    /// </summary>
    public sealed class WebOperationsProxy : IWebOperationsProxy
    {
        /// <summary>
        /// Downloads the resource with the specified URI to a local file.
        /// </summary>
        /// <param name="address">The URI from which to download data.</param>
        /// <param name="fileName">The name of the local file that is to receive the data.</param>
        public void DownloadFile(Uri address, string fileName)
        {
            using (var webClient = new WebClient())
            {
                webClient.DownloadFile(address, fileName);
            }
        }
    }
}
