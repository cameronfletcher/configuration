﻿// <copyright file="MemoryConfigurationProvider.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Configuration.Sdk;

    /// <summary>
    /// A memory configuration provider.
    /// </summary>
    public class MemoryConfigurationProvider : ConfigurationProviderBase
    {
        private readonly IDictionary<string, IAppSetting> appSettings;
        private readonly IDictionary<string, IConnectionString> connectionStrings;

        /// <summary>
        /// Initialises a new instance of the <see cref="MemoryConfigurationProvider"/> class.
        /// </summary>
        /// <param name="appSettings">The application settings.</param>
        /// <param name="connectionStrings">The connection strings.</param>
        /// <param name="logAppSetting">The application setting logging action.</param>
        /// <param name="logConnectionString">The connection string logging action.</param>
        public MemoryConfigurationProvider(
            IEnumerable<IAppSetting> appSettings,
            IEnumerable<IConnectionString> connectionStrings,
            Action<IAppSetting> logAppSetting, 
            Action<IConnectionString> logConnectionString)
            : base(logAppSetting, logConnectionString)
        {
            Guard.Against.Null(() => appSettings);
            Guard.Against.Null(() => connectionStrings);

            this.appSettings = appSettings.ToDictionary(appSetting => appSetting.Key);
            this.connectionStrings = connectionStrings.ToDictionary(connectionString => connectionString.Name);
        }

        /// <summary>
        /// Gets the application setting from the configuration.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>The application setting.</returns>
        protected override IAppSetting GetAppSettingFromConfiguration(string key)
        {
            IAppSetting appSetting;
            return this.appSettings.TryGetValue(key, out appSetting) ? appSetting : null;
        }

        /// <summary>
        /// Gets the connection string from the configuration.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <returns>The connection string.</returns>
        protected override IConnectionString GetConnectionStringFromConfiguration(string name)
        {
            IConnectionString connectionString;
            return this.connectionStrings.TryGetValue(name, out connectionString) ? connectionString : null;
        }
    }
}
