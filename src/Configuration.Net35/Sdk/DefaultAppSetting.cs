﻿// <copyright file="DefaultAppSetting.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    /// <summary>
    /// The default type of application setting.
    /// </summary>
    public class DefaultAppSetting : IAppSetting
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="DefaultAppSetting" /> class.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="value">The application setting value.</param>
        public DefaultAppSetting(string key, string value)
        {
            Guard.Against.Null(() => key);
            Guard.Against.Null(() => value);

            this.Key = key;
            this.Value = value;
        }

        /// <summary>
        /// Gets the key for the application setting.
        /// </summary>
        /// <value>The key for the application setting.</value>
        public string Key { get; private set; }

        /// <summary>
        /// Gets the value of the application setting.
        /// </summary>
        /// <value>The value of the application setting.</value>
        public string Value { get; private set; }
    }
}
