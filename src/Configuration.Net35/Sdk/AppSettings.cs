﻿// <copyright file="AppSettings.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// The application settings indexed by their keys.
    /// </summary>
    /// <remarks>Provided for backwards compatibility with .NET configuration only.</remarks>
    [Obsolete("Provided for backwards compatibility with .NET configuration only.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class AppSettings
    {
        private readonly IConfigurationProvider configProvider;

        /// <summary>
        /// Initialises a new instance of the <see cref="AppSettings" /> class.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        public AppSettings(IConfigurationProvider configProvider)
        {
            Guard.Against.Null(() => configProvider);

            this.configProvider = configProvider;
        }

        /// <summary>
        /// Gets the application setting value associated with the specified key.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>The application setting value.</returns>
        public string this[string key]
        {
            get
            {
                var setting = this.configProvider.GetAppSetting(key, true);
                return setting == null ? null : setting.AsString();
            }
        }
    }
}
