﻿// <copyright file="ConnectionStringSettings.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// The connection string settings indexed by their names.
    /// </summary>
    /// <remarks>Provided for backwards compatibility with .NET configuration only.</remarks>
    [Obsolete("Provided for backwards compatibility with .NET configuration only.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class ConnectionStringSettings
    {
        private readonly IConfigurationProvider configProvider;

        /// <summary>
        /// Initialises a new instance of the <see cref="ConnectionStringSettings" /> class.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        public ConnectionStringSettings(IConfigurationProvider configProvider)
        {
            Guard.Against.Null(() => configProvider);

            this.configProvider = configProvider;
        }

        /// <summary>
        /// Gets the connection string setting associated with the specified name.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <returns>The connection string setting.</returns>
        public IConnectionStringSetting this[string name]
        {
            get
            {
                var setting = this.configProvider.GetConnectionString(name, true);
                return setting == null ? null  : new DefaultConnectionStringSetting(setting.Name, setting.Value, setting.ProviderName);
            }
        }
    }
}
