﻿// <copyright file="IConnectionStringSetting.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Sdk
{
    using System;
    using System.ComponentModel;

    /// <summary>
    /// Exposes the public members of the connection string settings.
    /// </summary>
    /// <remarks>Provided for backwards compatibility with .NET configuration only.</remarks>
    [Obsolete("Provided for backwards compatibility with .NET configuration only.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IConnectionStringSetting
    {
        /// <summary>
        /// Gets the name of the connection string.
        /// </summary>
        /// <value>The name of the connection string.</value>
        string Name { get; }

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        string ConnectionString { get; }

        /// <summary>
        /// Gets the name of the provider for the connection string.
        /// </summary>
        /// <value>The name of the provider for the connection string.</value>
        string ProviderName { get; }
    }
}
