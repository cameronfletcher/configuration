﻿// <copyright file="IConfigurationProvider.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using Configuration.Sdk;

    /// <summary>
    /// Exposes the public members of a configuration provider.
    /// </summary>
    public interface IConfigurationProvider : IFluentExtension
    {
        /// <summary>
        /// Gets the application setting.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>The application setting.</returns>
        IAppSetting GetAppSetting(string key);

        /// <summary>
        /// Gets the application setting.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="allowNull">If set to <c>true</c> then allows a null application setting to be returned.</param>
        /// <returns>The application setting.</returns>
        IAppSetting GetAppSetting(string key, bool allowNull);

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <returns>The connection string.</returns>
        IConnectionString GetConnectionString(string name);

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <param name="allowNull">If set to <c>true</c> then allows a null connection string to be returned.</param>
        /// <returns>The connection string.</returns>
        IConnectionString GetConnectionString(string name, bool allowNull);
    }
}
