﻿// <copyright file="AppSettingExtensions.AsUri.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.Uri"/> extension methods for the <see cref="Configuration.IAppSetting"/> type.
    /// </summary>
    public static partial class AppSettingExtensions
    {
        /// <summary>
        /// Converts the application setting value to its <see cref="System.Uri"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <returns>Returns a <see cref="System.Uri"/> value.</returns>
        public static Uri AsUri(this IAppSetting appSetting)
        {
            Guard.Against.Null(() => appSetting);

            return new Uri(appSetting.Value);
        }

        /// <summary>
        /// Converts the application setting value to its <see cref="System.Uri"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <param name="uriKind">Specifies the kind of URI.</param>
        /// <returns>Returns a <see cref="System.Uri"/> value.</returns>
        public static Uri AsUri(this IAppSetting appSetting, UriKind uriKind)
        {
            Guard.Against.Null(() => appSetting);

            return new Uri(appSetting.Value, uriKind);
        }

        /// <summary>
        /// Converts the application setting value to its <see cref="System.Uri"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <param name="relativeUri">The relative URI to add to the base <see cref="System.Uri"/>.</param>
        /// <returns>Returns a <see cref="System.Uri"/> value.</returns>
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "1#", Justification = "By design.")]
        [SuppressMessage("Microsoft.Usage", "CA2234:PassSystemUriObjectsInsteadOfStrings", Justification = "By design.")]
        public static Uri AsUri(this IAppSetting appSetting, string relativeUri)
        {
            Guard.Against.Null(() => appSetting);

            var baseUri = new Uri(appSetting.Value, UriKind.Absolute);
            return new Uri(baseUri, relativeUri);
        }
    }
}
