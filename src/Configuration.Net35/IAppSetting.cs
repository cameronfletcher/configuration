﻿// <copyright file="IAppSetting.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using Configuration.Sdk;

    /// <summary>
    /// Exposes the public members of an application setting.
    /// </summary>
    public interface IAppSetting : IFluentExtension
    {
        /// <summary>
        /// Gets the key for the application setting.
        /// </summary>
        /// <value>The key for the application setting.</value>
        string Key { get; }

        /// <summary>
        /// Gets the value of the application setting.
        /// </summary>
        /// <value>The value of the application setting.</value>
        string Value { get; }
    }
}
