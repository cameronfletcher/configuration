﻿// <copyright file="AppSettingExtensions.AsString.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.String"/> extension methods for the <see cref="Configuration.IAppSetting"/> type.
    /// </summary>
    public static partial class AppSettingExtensions
    {
        /// <summary>
        /// Converts the application setting value to its <see cref="System.String"/> equivalent.
        /// </summary>
        /// <param name="appSetting">The application setting containing the value to convert.</param>
        /// <returns>Returns a <see cref="System.String"/> value.</returns>
        public static string AsString(this IAppSetting appSetting)
        {
            Guard.Against.Null(() => appSetting);

            return appSetting.Value;
        }
    }
}
