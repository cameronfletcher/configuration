﻿// <copyright file="MemoryConfiguration.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System.Collections.Generic;
    using Configuration.Sdk;

    /// <summary>
    /// The memory configuration.
    /// </summary>
    public class MemoryConfiguration : ConfigurationBase<DefaultConfiguration>, IConfiguration
    {
        private readonly Dictionary<string, IAppSetting> appSettings = new Dictionary<string, IAppSetting>();
        private readonly Dictionary<string, IConnectionString> connectionStrings = new Dictionary<string, IConnectionString>();

        /// <summary>
        /// Creates the configuration provider.
        /// </summary>
        /// <returns>The configuration provider.</returns>
        public IConfigurationProvider CreateProvider()
        {
            return new MemoryConfigurationProvider(
                this.appSettings.Values, 
                this.connectionStrings.Values, 
                this.LogAppSetting, 
                this.LogConnectionString);
        }

        /// <summary>
        /// Adds an application setting to the in-memory application settings.
        /// </summary>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="value">The application setting value.</param>
        /// <returns>The canned configuration.</returns>
        public MemoryConfiguration WithAppSetting(string key, string value)
        {
            this.appSettings.Add(key, new DefaultAppSetting(key, value));

            return this;
        }

        /// <summary>
        /// Adds an connection string to the in-memory connection strings.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <param name="value">The connection string value.</param>
        /// <returns>The canned configuration.</returns>
        public MemoryConfiguration WithConnectionString(string name, string value)
        {
            return this.WithConnectionString(name, value, null);
        }

        /// <summary>
        /// Adds an connection string to the in-memory connection strings.
        /// </summary>
        /// <param name="name">The name of the connection string.</param>
        /// <param name="value">The connection string value.</param>
        /// <param name="providerName">The name of the provider for the connection string.</param>
        /// <returns>The canned configuration.</returns>
        public MemoryConfiguration WithConnectionString(string name, string value, string providerName)
        {
            this.connectionStrings.Add(name, new DefaultConnectionString(name, value, providerName));

            return this;
        }
    }
}
