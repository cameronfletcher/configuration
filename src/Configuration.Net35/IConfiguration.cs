﻿// <copyright file="IConfiguration.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using Configuration.Sdk;

    /// <summary>
    /// Exposes the public members of a configuration.
    /// </summary>
    public interface IConfiguration : IFluentExtension
    {
        /// <summary>
        /// Creates the configuration provider.
        /// </summary>
        /// <returns>The configuration provider.</returns>
        IConfigurationProvider CreateProvider();
    }
}
