// <copyright file="GlobalSuppressions.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage(
    "Microsoft.Design", 
    "CA1020:AvoidNamespacesWithFewTypes", 
    Scope = "namespace", 
    Target = "Configuration.Advanced", 
    Justification = "By design.")]
