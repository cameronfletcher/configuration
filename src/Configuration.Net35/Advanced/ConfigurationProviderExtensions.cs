﻿// <copyright file="ConfigurationProviderExtensions.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Advanced
{
    using System;
    using System.ComponentModel;
    using Configuration.Sdk;

    /// <summary>
    /// Provides extension methods for the <see cref="Configuration.IConfigurationProvider"/> type.
    /// </summary>
    public static partial class ConfigurationProviderExtensions
    {
        /// <summary>
        /// Gets and converts the application setting value to its formatted <see cref="System.String" /> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="args">A <see cref="System.Object"/> array containing zero or more objects to format.</param>
        /// <returns>Returns a formatted <see cref="System.String" /> value.</returns>
        public static string GetString(this IConfigurationProvider configProvider, string key, params object[] args)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsString(args);
        }

        /// <summary>
        /// Gets and converts the application setting value to its formatted <see cref="System.String"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <param name="args">A <see cref="System.Object"/> array containing zero or more objects to format.</param>
        /// <returns>Returns a formatted <see cref="System.String"/> value.</returns>
        public static string GetString(this IConfigurationProvider configProvider, string key, IFormatProvider provider, params object[] args)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsString(provider, args);
        }
    }
}
