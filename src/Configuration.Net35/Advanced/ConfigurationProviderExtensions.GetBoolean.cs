﻿// <copyright file="ConfigurationProviderExtensions.GetBoolean.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Advanced
{
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.Boolean"/> extension methods for the <see cref="Configuration.IConfigurationProvider"/> type.
    /// </summary>
    public static partial class ConfigurationProviderExtensions
    {
        /// <summary>
        /// Gets and converts application setting value to its <see cref="System.Boolean"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>Returns a <see cref="System.Boolean"/> value.</returns>
        public static bool GetBoolean(this IConfigurationProvider configProvider, string key)
        {
            Guard.Against.Null(() => configProvider);
            
            return configProvider.GetAppSetting(key).AsBoolean();
        }
    }
}
