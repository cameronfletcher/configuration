﻿// <copyright file="ConfigurationProviderExtensions.GetString.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Advanced
{
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.String"/> extension methods for the <see cref="Configuration.IConfigurationProvider"/> type.
    /// </summary>
    public static partial class ConfigurationProviderExtensions
    {
        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.String"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>Returns a <see cref="System.String"/> value.</returns>
        public static string GetString(this IConfigurationProvider configProvider, string key)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsString();
        }
    }
}
