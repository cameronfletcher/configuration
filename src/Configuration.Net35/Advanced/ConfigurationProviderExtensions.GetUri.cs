﻿// <copyright file="ConfigurationProviderExtensions.GetUri.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Advanced
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.Uri"/> extension methods for the <see cref="Configuration.IConfigurationProvider"/> type.
    /// </summary>
    public static partial class ConfigurationProviderExtensions
    {
        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Uri"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>Returns a <see cref="System.Uri"/> value.</returns>
        public static Uri GetUri(this IConfigurationProvider configProvider, string key)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsUri();
        }

        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Uri"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="uriKind">Specifies the kind of URI.</param>
        /// <returns>Returns a <see cref="System.Uri"/> value.</returns>
        public static Uri GetUri(this IConfigurationProvider configProvider, string key, UriKind uriKind)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsUri(uriKind);
        }

        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Uri"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="relativeUri">The relative URI to add to the base <see cref="System.Uri"/>.</param>
        /// <returns>Returns a <see cref="System.Uri"/> value.</returns>
        [SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "2#", Justification = "By design.")]
        public static Uri GetUri(this IConfigurationProvider configProvider, string key, string relativeUri)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsUri(relativeUri);
        }
    }
}
