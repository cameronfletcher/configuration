﻿// <copyright file="ConfigurationProviderExtensions.GetDecimal.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Advanced
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.Decimal"/> extension methods for the <see cref="Configuration.IConfigurationProvider"/> type.
    /// </summary>
    public static partial class ConfigurationProviderExtensions
    {
        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Decimal"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>Returns a <see cref="System.Decimal"/> value.</returns>
        [SuppressMessage(
            "Microsoft.Globalization", 
            "CA1305:SpecifyIFormatProvider", 
            MessageId = "Configuration.AppSettingExtensions.AsDecimal(Configuration.IAppSetting)",
            Justification = "By design.")]
        public static decimal GetDecimal(this IConfigurationProvider configProvider, string key)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsDecimal();
        }

        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Decimal"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="style">
        /// A bitwise combination of <see cref="System.Globalization.NumberStyles"/> values that indicates the
        /// style elements that can be present in the application setting value.
        /// </param>
        /// <returns>Returns a <see cref="System.Decimal"/> value.</returns>
        [SuppressMessage(
            "Microsoft.Globalization", 
            "CA1305:SpecifyIFormatProvider", 
            MessageId = "Configuration.AppSettingExtensions.AsDecimal(Configuration.IAppSetting,System.Globalization.NumberStyles)",
            Justification = "By design.")]
        public static decimal GetDecimal(this IConfigurationProvider configProvider, string key, NumberStyles style)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsDecimal(style);
        }

        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Decimal"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.Decimal"/> value.</returns>
        public static decimal GetDecimal(this IConfigurationProvider configProvider, string key, IFormatProvider provider)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsDecimal(provider);
        }

        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Decimal"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="style">
        /// A bitwise combination of <see cref="System.Globalization.NumberStyles"/> values that indicates the
        /// style elements that can be present in the application setting value.
        /// </param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.Decimal"/> value.</returns>
        public static decimal GetDecimal(this IConfigurationProvider configProvider, string key, NumberStyles style, IFormatProvider provider)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsDecimal(style, provider);
        }
    }
}
