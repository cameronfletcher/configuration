﻿// <copyright file="ConfigurationProviderExtensions.GetInt16.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Advanced
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.Int16"/> extension methods for the <see cref="Configuration.IConfigurationProvider"/> type.
    /// </summary>
    public static partial class ConfigurationProviderExtensions
    {
        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Int16"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>Returns a <see cref="System.Int16"/> value.</returns>
        [SuppressMessage(
            "Microsoft.Globalization", 
            "CA1305:SpecifyIFormatProvider", 
            MessageId = "Configuration.AppSettingExtensions.AsInt16(Configuration.IAppSetting)",
            Justification = "By design.")]
        public static short GetInt16(this IConfigurationProvider configProvider, string key)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsInt16();
        }

        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Int16"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="style">
        /// A bitwise combination of <see cref="System.Globalization.NumberStyles"/> values that indicates the
        /// style elements that can be present in the application setting value.
        /// </param>
        /// <returns>Returns a <see cref="System.Int16"/> value.</returns>
        [SuppressMessage(
            "Microsoft.Globalization", 
            "CA1305:SpecifyIFormatProvider", 
            MessageId = "Configuration.AppSettingExtensions.AsInt16(Configuration.IAppSetting,System.Globalization.NumberStyles)",
            Justification = "By design.")]
        public static short GetInt16(this IConfigurationProvider configProvider, string key, NumberStyles style)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsInt16(style);
        }

        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Int16"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.Int16"/> value.</returns>
        public static short GetInt16(this IConfigurationProvider configProvider, string key, IFormatProvider provider)
        {
            Guard.Against.Null(() => configProvider);
            
            return configProvider.GetAppSetting(key).AsInt16(provider);
        }

        /// <summary>
        /// Gets and converts the application setting value to its <see cref="System.Int16"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="style">
        /// A bitwise combination of <see cref="System.Globalization.NumberStyles"/> values that indicates the
        /// style elements that can be present in the application setting value.
        /// </param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.Int16"/> value.</returns>
        public static short GetInt16(this IConfigurationProvider configProvider, string key, NumberStyles style, IFormatProvider provider)
        {
            Guard.Against.Null(() => configProvider);
            
            return configProvider.GetAppSetting(key).AsInt16(style, provider);
        }
    }
}
