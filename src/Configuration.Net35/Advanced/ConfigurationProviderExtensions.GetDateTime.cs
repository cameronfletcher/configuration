﻿// <copyright file="ConfigurationProviderExtensions.GetDateTime.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration.Advanced
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using Configuration.Sdk;

    /// <summary>
    /// Provides <see cref="System.DateTime"/> extension methods for the <see cref="Configuration.IConfigurationProvider"/> type.
    /// </summary>
    public static partial class ConfigurationProviderExtensions
    {
        /// <summary>
        /// Gets and converts application setting value to its <see cref="System.DateTime"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <returns>Returns a <see cref="System.DateTime"/> value.</returns>
        [SuppressMessage(
            "Microsoft.Globalization", 
            "CA1305:SpecifyIFormatProvider", 
            MessageId = "Configuration.AppSettingExtensions.AsDateTime(Configuration.IAppSetting)",
            Justification = "By design.")]
        public static DateTime GetDateTime(this IConfigurationProvider configProvider, string key)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsDateTime();
        }

        /// <summary>
        /// Gets and converts application setting value to its <see cref="System.DateTime"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.DateTime"/> value.</returns>
        public static DateTime GetDateTime(this IConfigurationProvider configProvider, string key, IFormatProvider provider)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsDateTime(provider);
        }

        /// <summary>
        /// Gets and converts application setting value to its <see cref="System.DateTime"/> equivalent.
        /// </summary>
        /// <param name="configProvider">The configuration provider.</param>
        /// <param name="key">The key for the application setting.</param>
        /// <param name="styles">
        /// A bitwise combination of the enumeration values that indicates the style elements that can be present in  the application setting value
        /// for the parse operation to succeed and that defines how to interpret the parsed date in relation to the current time zone or the current
        /// date. A typical value to specify is <see cref="System.Globalization.DateTimeStyles.None"/>.
        /// </param>
        /// <param name="provider">
        /// A <see cref="System.IFormatProvider"/> implementation that supplies culture-specific formatting information.
        /// </param>
        /// <returns>Returns a <see cref="System.DateTime"/> value.</returns>
        public static DateTime GetDateTime(this IConfigurationProvider configProvider, string key, DateTimeStyles styles, IFormatProvider provider)
        {
            Guard.Against.Null(() => configProvider);

            return configProvider.GetAppSetting(key).AsDateTime(styles, provider);
        }
    }
}
