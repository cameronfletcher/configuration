﻿// <copyright file="NetworkConfiguration.cs" company="Configuration contributors">
//  Copyright (c) Configuration contributors. All rights reserved.
// </copyright>

namespace Configuration
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.Net;
    using Configuration.Sdk;

    /// <summary>
    /// The network configuration.
    /// </summary>
    public class NetworkConfiguration : ConfigurationBase<NetworkConfiguration>, IConfiguration
    {
        private readonly IFileOperationsProxy fileOperationsProxy;
        private readonly IWebOperationsProxy webOperationsProxy;
        private readonly Uri configurationUri;
        
        private string cacheFilename;

        /// <summary>
        /// Initialises a new instance of the <see cref="NetworkConfiguration" /> class.
        /// </summary>
        /// <param name="configurationUriString">The configuration URI string.</param>
        [SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "string", Justification = "By design.")]
        public NetworkConfiguration(string configurationUriString)
            : this(new Uri(configurationUriString))
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NetworkConfiguration" /> class.
        /// </summary>
        /// <param name="configurationUri">The configuration URI.</param>
        public NetworkConfiguration(Uri configurationUri)
            : this(new FileOperationsProxy(), new WebOperationsProxy(), configurationUri)
        {
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="NetworkConfiguration" /> class.
        /// </summary>
        /// <param name="fileOperationsProxy">The file operations proxy.</param>
        /// <param name="webOperationsProxy">The web operations proxy.</param>
        /// <param name="configurationUri">The configuration URI.</param>
        protected internal NetworkConfiguration(IFileOperationsProxy fileOperationsProxy, IWebOperationsProxy webOperationsProxy, Uri configurationUri)
        {
            Guard.Against.Null(() => fileOperationsProxy);
            Guard.Against.Null(() => webOperationsProxy);
            Guard.Against.Null(() => configurationUri);

            this.fileOperationsProxy = fileOperationsProxy;
            this.webOperationsProxy = webOperationsProxy;
            this.configurationUri = configurationUri;
        }

        /// <summary>
        /// Creates the configuration provider.
        /// </summary>
        /// <returns>The configuration provider.</returns>
        public IConfigurationProvider CreateProvider()
        {
            var tempFilename = this.fileOperationsProxy.GetTempFileName();
            var configFileName = this.cacheFilename ?? tempFilename;

            var webException = default(WebException);
            try
            {
                this.webOperationsProxy.DownloadFile(this.configurationUri, tempFilename);
            }
            catch (WebException ex)
            {
                webException = ex;
            }

            if (webException == null && tempFilename != configFileName)
            {
                this.fileOperationsProxy.FileCopy(tempFilename, configFileName, true);
            }

            if (!this.fileOperationsProxy.FileExists(configFileName))
            {
                throw new ConfigurationException(
                    string.Format(CultureInfo.InvariantCulture, "Unable to open network configuration from '{0}'.", this.configurationUri),
                    webException);
            }

            return new FileConfigurationProvider(new FileOperationsProxy(), configFileName, this.LogAppSetting, this.LogConnectionString);
        }

        /// <summary>
        /// Caches the contents of the configuration to the local disk with the specified filename.
        /// </summary>
        /// <param name="filename">The filename to use for the cache.</param>
        /// <returns>The network configuration.</returns>
        //// LINK (Cameron): http://stackoverflow.com/questions/2006795/fxcop-compound-word-should-be-treated-as-discrete-term
        [SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "filename", Justification = "This is a mess.")]
        public NetworkConfiguration CacheToDisk(string filename)
        {
            Guard.Against.Null(() => filename);

            this.cacheFilename = filename;

            return this;
        }
    }
}